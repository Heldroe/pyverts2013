from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^(?P<category_id>\d+)/$', 'products.views.view_category', name='category'),
    url(r'^$', 'products.views.all_categories', name='categories'),
    #url(r'^(?P<gallery_id>\d+)/item/', include('galleries.items.urls')),
    #url(r'^create/$', 'cats.views.create', name='create_cat'),
    #url(r'^edit/(?P<gallery_id>\d+)/$', 'galleries.views.edit', name='edit_gallery'),
)