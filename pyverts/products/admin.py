from django.contrib import admin

from products.models import Category, Attribute, Value

admin.site.register(Value)
admin.site.register(Category)
admin.site.register(Attribute)