from django.contrib.auth.decorators import login_required

from django.shortcuts import render

from products.models import Category, Attribute, Value

from django.shortcuts import get_object_or_404

from cats.models import Basket, Cat, Nature

from semantics3 import Products

from urllib2 import HTTPError

@login_required
def all_categories (request):
    categories = Category.objects.all()
    return render(request, 'products/categories.html', {'categories': categories})

@login_required
def view_category (request, category_id):
    category = get_object_or_404(Category, id=category_id)
    attributes = Attribute.objects.filter(category=category)
    basket = Basket.objects.get(owner=request.user)
    cats = Cat.objects.filter(basket=basket)

    p = Products(
        api_key = "SEM3D5E095AA055A59E59D330F085655CBF1",
        api_secret = "MDBhMGE3MmQ0YWZiOTFhOTBlZGM3YTAwYTg1OWRhMjA"
    )

    p.products_field("cat_id", category.cat_id)

    mean_nature = {}
    i = 0

    mean_nature['calm'] = 0
    mean_nature['playful'] = 0
    mean_nature['cuddly'] = 0
    mean_nature['independent'] = 0
    mean_nature['noisy'] = 0
    mean_nature['kindness'] = 0
    mean_nature['socialness'] = 0
    mean_nature['scampish'] = 0
    mean_nature['robust'] = 0
    mean_nature['sexual_activity'] = 0
    mean_nature['tidyness'] = 0
    mean_nature['dirtyness'] = 0
    mean_nature['first_cat'] = 0
    mean_nature['originality'] = 0
    mean_nature['budget'] = 0

    for cat in cats:
        nature = cat.race.nature
        mean_nature['calm'] += nature.calm
        mean_nature['playful'] += nature.playful
        mean_nature['cuddly'] += nature.cuddly
        mean_nature['independent'] += nature.independent
        mean_nature['noisy'] += nature.noisy
        mean_nature['kindness'] += nature.kindness
        mean_nature['socialness'] += nature.socialness
        mean_nature['scampish'] += nature.scampish
        mean_nature['robust'] += nature.robust
        mean_nature['sexual_activity'] += nature.sexual_activity
        mean_nature['tidyness'] += nature.tidyness
        mean_nature['dirtyness'] += nature.dirtyness
        mean_nature['first_cat'] += nature.first_cat
        mean_nature['originality'] += nature.originality
        mean_nature['budget'] += nature.budget
        i += 1
    for key in mean_nature:
        mean_nature[key] = mean_nature[key] / i
    for attribute in attributes:
        values = Value.objects.filter(attribute=attribute)
        best_value = 0
        min_difference = 999999
        for value in values:
            nature = value.nature
            difference = 0
            if nature.calm >= 0:
                difference += abs(mean_nature['calm'] - nature.calm)
            if nature.playful >= 0:
                difference += abs(mean_nature['playful'] - nature.playful)
            if nature.cuddly >= 0:
                difference += abs(mean_nature['cuddly'] - nature.cuddly)
            if nature.independent >= 0:
                difference += abs(mean_nature['independent'] - nature.independent)
            if nature.noisy >= 0:
                difference += abs(mean_nature['noisy'] - nature.noisy)
            if nature.kindness >= 0:
                difference += abs(mean_nature['kindness'] - nature.kindness)
            if nature.socialness >= 0:
                difference += abs(mean_nature['socialness'] - nature.socialness)
            if nature.scampish >= 0:
                difference += abs(mean_nature['scampish'] - nature.scampish)
            if nature.robust >= 0:
                difference += abs(mean_nature['robust'] - nature.robust)
            if nature.sexual_activity >= 0:
                difference += abs(mean_nature['sexual_activity'] - nature.sexual_activity)
            if nature.tidyness >= 0:
                difference += abs(mean_nature['tidyness'] - nature.tidyness)
            if nature.dirtyness >= 0:
                difference += abs(mean_nature['dirtyness'] - nature.dirtyness)
            if nature.first_cat >= 0:
                difference += abs(mean_nature['first_cat'] - nature.first_cat)
            if nature.originality >= 0:
                difference += abs(mean_nature['originality'] - nature.originality)
            if nature.budget >= 0:
                difference += abs(mean_nature['budget'] - nature.budget)
            if difference < min_difference:
                min_difference = difference
                best_value = value
        print best_value.name
        print min_difference
        if best_value.name:
            p.products_field(attribute.name, best_value.name)
        if best_value.value:
            p.products_field(attribute.name, best_value.value)
        if best_value.min_value:
            p.products_field(attribute.name, "gte", best_value.min_value)
        if best_value.max_value:
            p.products_field(attribute.name, "lte", best_value.max_value)
    results = p.get_products();
    try:
        return render(request, 'products/view.html', {'results': results,
                                                    'category': category})
    except HTTPError:
        return render(request, 'products/view.html', {'results': results,
                                                    'category': category,
                                                    'no_img': True})