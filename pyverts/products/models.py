from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from cats.models import Nature

class Category(models.Model):
    name = models.CharField(max_length=100, blank=False)
    cat_id = models.IntegerField(default=0)

    def __unicode__(self):
        return self.name

class Attribute(models.Model):
    category = models.ForeignKey(Category)
    name = models.CharField(max_length=100, blank=False)

    def __unicode__(self):
        return self.name+" ("+self.category.name+")"

class Value(models.Model):
    attribute = models.ForeignKey(Attribute)
    name = models.CharField(max_length=100, blank=False)
    value = models.IntegerField(blank=True, null=True)
    min_value = models.IntegerField(blank=True, null=True)
    max_value = models.IntegerField(blank=True, null=True)
    nature = models.ForeignKey(Nature)

    def __unicode__(self):
        return self.name+" ("+self.attribute.category.name+" - "+self.attribute.name+")"