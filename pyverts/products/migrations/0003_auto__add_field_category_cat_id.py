# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Category.cat_id'
        db.add_column(u'products_category', 'cat_id',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Category.cat_id'
        db.delete_column(u'products_category', 'cat_id')


    models = {
        u'cats.nature': {
            'Meta': {'object_name': 'Nature'},
            'budget': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'calm': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'cuddly': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'dirtyness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'first_cat': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'fluffyness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'independent': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'kindness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'noisy': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'originality': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'playful': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'robust': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'scampish': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'sexual_activity': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'socialness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'tidyness': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'products.attribute': {
            'Meta': {'object_name': 'Attribute'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['products.Category']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'products.category': {
            'Meta': {'object_name': 'Category'},
            'cat_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'products.value': {
            'Meta': {'object_name': 'Value'},
            'attribute': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['products.Attribute']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_value': ('django.db.models.fields.IntegerField', [], {}),
            'min_value': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nature': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cats.Nature']"}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['products']