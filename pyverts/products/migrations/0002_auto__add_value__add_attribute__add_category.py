# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Value'
        db.create_table(u'products_value', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('attribute', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['products.Attribute'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
            ('min_value', self.gf('django.db.models.fields.IntegerField')()),
            ('max_value', self.gf('django.db.models.fields.IntegerField')()),
            ('nature', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cats.Nature'])),
        ))
        db.send_create_signal(u'products', ['Value'])

        # Adding model 'Attribute'
        db.create_table(u'products_attribute', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['products.Category'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'products', ['Attribute'])

        # Adding model 'Category'
        db.create_table(u'products_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'products', ['Category'])


    def backwards(self, orm):
        # Deleting model 'Value'
        db.delete_table(u'products_value')

        # Deleting model 'Attribute'
        db.delete_table(u'products_attribute')

        # Deleting model 'Category'
        db.delete_table(u'products_category')


    models = {
        u'cats.nature': {
            'Meta': {'object_name': 'Nature'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'playful': ('django.db.models.fields.IntegerField', [], {})
        },
        u'products.attribute': {
            'Meta': {'object_name': 'Attribute'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['products.Category']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'products.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'products.value': {
            'Meta': {'object_name': 'Value'},
            'attribute': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['products.Attribute']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_value': ('django.db.models.fields.IntegerField', [], {}),
            'min_value': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nature': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cats.Nature']"}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['products']