# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Value.min_value'
        db.alter_column(u'products_value', 'min_value', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'Value.value'
        db.alter_column(u'products_value', 'value', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'Value.max_value'
        db.alter_column(u'products_value', 'max_value', self.gf('django.db.models.fields.IntegerField')(null=True))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Value.min_value'
        raise RuntimeError("Cannot reverse this migration. 'Value.min_value' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Value.min_value'
        db.alter_column(u'products_value', 'min_value', self.gf('django.db.models.fields.IntegerField')())

        # User chose to not deal with backwards NULL issues for 'Value.value'
        raise RuntimeError("Cannot reverse this migration. 'Value.value' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Value.value'
        db.alter_column(u'products_value', 'value', self.gf('django.db.models.fields.IntegerField')())

        # User chose to not deal with backwards NULL issues for 'Value.max_value'
        raise RuntimeError("Cannot reverse this migration. 'Value.max_value' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Value.max_value'
        db.alter_column(u'products_value', 'max_value', self.gf('django.db.models.fields.IntegerField')())

    models = {
        u'cats.nature': {
            'Meta': {'object_name': 'Nature'},
            'budget': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'calm': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'cuddly': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'dirtyness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'first_cat': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'fluffyness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'independent': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'kindness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'noisy': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'originality': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'playful': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'robust': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'scampish': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'sexual_activity': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'socialness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'tidyness': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'products.attribute': {
            'Meta': {'object_name': 'Attribute'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['products.Category']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'products.category': {
            'Meta': {'object_name': 'Category'},
            'cat_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'products.value': {
            'Meta': {'object_name': 'Value'},
            'attribute': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['products.Attribute']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_value': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'min_value': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nature': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cats.Nature']"}),
            'value': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['products']