# -*- coding: utf-8 -*- 
from django.contrib.auth.decorators import login_required

from django.shortcuts import render
from galleries.models import Gallery
from galleries.forms import GalleryForm

from items.models import Item

from django.shortcuts import get_object_or_404

from django.shortcuts import redirect

from django.core.exceptions import PermissionDenied

from actstream import action
from actstream.models import actor_stream

from django.utils.translation import ugettext as _

@login_required
def view (request, gallery_id):
    gallery = get_object_or_404(Gallery, id=gallery_id)
    items = Item.objects.filter(gallery=gallery)
    gallery.items = items
    return render(request, 'galleries/view.html', {'gallery': gallery})

@login_required
def create (request):
    form = GalleryForm();
    if request.method == "POST":
        form = GalleryForm(request.POST)
        if form.is_valid():
            gallery = form.save(commit=False)
            gallery.owner = request.user
            gallery.save()
            action.send(request.user, verb=_(u"a créé"), action_object=gallery)
            return redirect(gallery)
    return render(request, 'galleries/create.html', {'form': form})

@login_required
def edit (request, gallery_id):
    gallery = get_object_or_404(Gallery, id=gallery_id)
    if gallery.owner != request.user:
        raise PermissionDenied
    form = GalleryForm(instance=gallery);
    if request.method == "POST":
        form = GalleryForm(request.POST, instance=gallery)
        if form.is_valid():
            gallery = form.save(commit=False)
            gallery.save()
            action.send(request.user, verb=_(u"a mis à jour"), action_object=gallery)
            return redirect(gallery)
    return render(request, 'galleries/edit.html', {'form': form})

    