from django.forms import ModelForm
from galleries.models import Gallery

class GalleryForm(ModelForm):
	class Meta:
		model = Gallery
		fields = ('name',)