from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from galleries.models import Gallery

class Item(models.Model):
    gallery = models.ForeignKey(Gallery)
    name = models.CharField(max_length=100, blank=True)
    
    def get_absolute_url(self):
        return reverse('item', args=[int(self.gallery.id), int(self.id)])

    def __unicode__(self):
        return self.name