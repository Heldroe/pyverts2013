# -*- coding: utf-8 -*- 
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from galleries.models import Gallery

from .models import Item
from .forms import ItemForm

from django.shortcuts import get_object_or_404

from django.core.exceptions import PermissionDenied

from django.shortcuts import redirect

from actstream import action
from actstream.models import actor_stream

from django.utils.translation import ugettext as _

@login_required
def create (request, gallery_id):
    gallery = get_object_or_404(Gallery, id=gallery_id)
    if gallery.owner != request.user:
        raise PermissionDenied
    form = ItemForm();
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.gallery = gallery
            item.save()
            action.send(request.user, verb=_(u"a ajouté"), action_object=item, target=gallery)
            return redirect(item)
    return render(request, 'items/create.html', {'form': form, 'gallery': gallery})

def view (request, gallery_id, item_id):
    item = get_object_or_404(Item, id=item_id)
    return render(request, 'items/view.html', {'item': item})


@login_required
def edit (request, gallery_id, item_id):
    item = get_object_or_404(Item, id=item_id)
    if item.gallery.owner != request.user:
        raise PermissionDenied
    form = ItemForm(instance=item);
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save(commit=False)
            item.save()
            action.send(request.user, verb=_(u"a mis à jour"), action_object=item, target=item.gallery)
            return redirect(item)
    return render(request, 'items/edit.html', {'form': form, 'item': item})