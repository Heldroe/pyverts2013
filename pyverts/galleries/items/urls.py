from django.conf.urls import patterns, include, url

from . import views

urlpatterns = patterns('',
    url(r'^(?P<item_id>\d+)/$', views.view, name='item'),
    url(r'^create/$', views.create, name='create_item'),
    url(r'^edit/(?P<item_id>\d+)/$', views.edit, name='edit_item'),
)