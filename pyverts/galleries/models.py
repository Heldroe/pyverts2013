from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

class Gallery(models.Model):
    owner = models.ForeignKey(User)
    name = models.CharField(max_length=100, blank=True)

    def get_absolute_url(self):
        return reverse('gallery', args=[int(self.id)])

    def __unicode__(self):
        return self.name