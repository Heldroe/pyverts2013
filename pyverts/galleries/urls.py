from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^(?P<gallery_id>\d+)/$', 'galleries.views.view', name='gallery'),
    url(r'^(?P<gallery_id>\d+)/item/', include('galleries.items.urls')),
    url(r'^create/$', 'galleries.views.create', name='create_gallery'),
    url(r'^edit/(?P<gallery_id>\d+)/$', 'galleries.views.edit', name='edit_gallery'),
)