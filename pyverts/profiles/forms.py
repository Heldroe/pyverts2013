from django.forms import ModelForm
from profiles.models import UserProfile

class ProfileEditForm(ModelForm):
	class Meta:
		model = UserProfile
		fields = ('some_data',)