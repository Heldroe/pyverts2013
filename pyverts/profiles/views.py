# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect

from django.contrib.auth.models import User

from profiles.models import UserProfile
from profiles.forms import ProfileEditForm

from galleries.models import Gallery

from actstream import action
from actstream.models import actor_stream, user_stream
from actstream.actions import follow, unfollow

from django.utils.translation import ugettext as _

from cats.models import Basket, Cat

@login_required
def my_profile (request):
    return view(request, request.user.id)

def view (request, user_id):
    profile_user = get_object_or_404(User, id=user_id)
    galleries = Gallery.objects.filter(owner=profile_user)
    user_activity = actor_stream(profile_user)
    basket = Basket.objects.get(owner=profile_user)
    cats = Cat.objects.filter(basket=basket)

    mean_nature = {}
    i = 0

    mean_nature['calm'] = 0
    mean_nature['playful'] = 0
    mean_nature['cuddly'] = 0
    mean_nature['independent'] = 0
    mean_nature['noisy'] = 0
    mean_nature['kindness'] = 0
    mean_nature['socialness'] = 0
    mean_nature['scampish'] = 0
    mean_nature['robust'] = 0
    mean_nature['sexual_activity'] = 0
    mean_nature['tidyness'] = 0
    mean_nature['dirtyness'] = 0
    mean_nature['first_cat'] = 0
    mean_nature['originality'] = 0
    mean_nature['budget'] = 0

    for cat in cats:
        nature = cat.race.nature
        mean_nature['calm'] += nature.calm
        mean_nature['playful'] += nature.playful
        mean_nature['cuddly'] += nature.cuddly
        mean_nature['independent'] += nature.independent
        mean_nature['noisy'] += nature.noisy
        mean_nature['kindness'] += nature.kindness
        mean_nature['socialness'] += nature.socialness
        mean_nature['scampish'] += nature.scampish
        mean_nature['robust'] += nature.robust
        mean_nature['sexual_activity'] += nature.sexual_activity
        mean_nature['tidyness'] += nature.tidyness
        mean_nature['dirtyness'] += nature.dirtyness
        mean_nature['first_cat'] += nature.first_cat
        mean_nature['originality'] += nature.originality
        mean_nature['budget'] += nature.budget
        i += 1
    for key in mean_nature:
        mean_nature[key] = int(mean_nature[key] / max(1,i))

    #profile_stream = actor_stream(profile_user)
    return render(request, 'profiles/view.html', {'profile_user': profile_user,
                                                  'galleries': galleries,
                                                  'user_activity': user_activity,
                                                  'stats': mean_nature,
                                                  'cats': cats})

@login_required
def edit (request):
    profile = UserProfile.objects.get(user=request.user)
    base_form = ProfileEditForm(instance=profile)
    #avatar_form = ProfileAvatarForm(instance=profile)
    if request.method == "POST":
        #if 'base_form' in request.POST:
        base_form = ProfileEditForm(request.POST, instance=profile)
        if base_form.is_valid():
            profile = base_form.save(commit=False)
            profile.save()
                # action.send(request.user, verb=_(u"a modifié sa description"))
        # elif 'avatar_form' in request.POST:
        #     avatar_form = ProfileAvatarForm(request.POST, request.FILES, instance=profile)
        #     if avatar_form.is_valid():
        #         profile = avatar_form.save(commit=False)
        #         profile.save()
        #         action.send(request.user, verb=_(u"a modifié sa photo de profil"))
    return render(request, 'profiles/edit.html', {'base_form': base_form})
                                                 # 'avatar_form': avatar_form})