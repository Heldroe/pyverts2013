jQuery(document).ready(function ($) {
    $('.race').click(function () {
        //alert('bla');
        $(this).children('input:radio').prop("checked", true);
        $(".race").attr("class", "thumbnail race");
        $(this).attr("class", "thumbnail race race-selected");
        var calm = $(this).children(".calm").val();
        $("#calm").attr("aria-valuenow", calm);
        $("#calm").attr("style", "width: "+calm+"%");

        var playful = $(this).children(".playful").val();
        $("#playful").attr("aria-valuenow", playful);
        $("#playful").attr("style", "width: "+playful+"%");

        var cuddly = $(this).children(".cuddly").val();
        $("#cuddly").attr("aria-valuenow", cuddly);
        $("#cuddly").attr("style", "width: "+cuddly+"%");

        var independent = $(this).children(".independent").val();
        $("#independent").attr("aria-valuenow", independent);
        $("#independent").attr("style", "width: "+independent+"%");

        var noisy = $(this).children(".noisy").val();
        $("#noisy").attr("aria-valuenow", noisy);
        $("#noisy").attr("style", "width: "+noisy+"%");

        var kindness = $(this).children(".kindness").val();
        $("#kindness").attr("aria-valuenow", kindness);
        $("#kindness").attr("style", "width: "+kindness+"%");

        var socialness = $(this).children(".socialness").val();
        $("#socialness").attr("aria-valuenow", socialness);
        $("#socialness").attr("style", "width: "+socialness+"%");

        var tidyness = $(this).children(".tidyness").val();
        $("#tidyness").attr("aria-valuenow", tidyness);
        $("#tidyness").attr("style", "width: "+tidyness+"%");

        var originality = $(this).children(".originality").val();
        $("#originality").attr("aria-valuenow", originality);
        $("#originality").attr("style", "width: "+originality+"%");

        var budget = $(this).children(".budget").val();
        $("#budget").attr("aria-valuenow", budget);
        $("#budget").attr("style", "width: "+budget+"%");

    });
    // $('input:checkbox').click(function () {
    //     $(this).closest('.photo').toggleClass('selected');
    // });
});
