from django.shortcuts import render
from django.shortcuts import redirect
from django.core.urlresolvers import reverse

def home (request):
    if request.user.is_authenticated():
        return redirect('my_profile')
    else:
        return render(request, 'home.html', {'home': True})
