# -*- coding: utf-8 -*- 
from django.contrib.auth.decorators import login_required

from django.shortcuts import render

from cats.models import Basket, Cat, Race, Nature
from cats.forms import CatForm

from django.shortcuts import redirect

from django.shortcuts import get_object_or_404

from django.core.exceptions import PermissionDenied

import json
from django.http import HttpResponse

from sorl.thumbnail import get_thumbnail

@login_required
def create (request):
    form = CatForm();
    basket = Basket.objects.get(owner=request.user)
    races = Race.objects.all()
    if request.method == "POST":
        form = CatForm(request.POST)
        if form.is_valid():
            cat = form.save(commit=False)
            cat.basket = basket
            cat.save()
            return redirect('my_profile')
    return render(request, 'cats/create.html', {'form': form, 'races': races})

@login_required
def delete (request, cat_id):
    cat = get_object_or_404(Cat, id=cat_id)
    if cat.basket.owner != request.user:
        raise PermissionDenied
    cat.delete()
    return redirect('my_profile')

def json_races (request):
    races = Race.objects.all()
    obj = {'races': []}
    for race in races:
        race_obj = {}
        race_obj['name'] = race.name
        race_obj['img'] = get_thumbnail(race.young_image, '150x150', crop='center').url
        race_obj['calm'] = race.nature.calm
        race_obj['playful'] = race.nature.playful
        race_obj['cuddly'] = race.nature.cuddly
        race_obj['independent'] = race.nature.independent
        race_obj['noisy'] = race.nature.noisy
        race_obj['kindness'] = race.nature.kindness
        race_obj['socialness'] = race.nature.socialness
        race_obj['scampish'] = race.nature.scampish
        race_obj['robust'] = race.nature.robust
        race_obj['sexual_activity'] = race.nature.sexual_activity
        race_obj['tidyness'] = race.nature.tidyness
        race_obj['fluffyness'] = race.nature.fluffyness
        race_obj['dirtyness'] = race.nature.dirtyness
        race_obj['first_cat'] = race.nature.first_cat
        race_obj['originality'] = race.nature.originality
        race_obj['budget'] = race.nature.budget
        obj['races'].append(race_obj)
    return HttpResponse(json.dumps(obj, indent=4), content_type="application/json")
