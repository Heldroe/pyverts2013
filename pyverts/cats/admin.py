from django.contrib import admin

from cats.models import Nature, Race, Cat, Basket

admin.site.register(Nature)
admin.site.register(Race)
admin.site.register(Basket)
admin.site.register(Cat)