from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from sorl.thumbnail import ImageField

from django.db.models.signals import post_save

class Basket(models.Model):
    owner = models.OneToOneField(User)

class Nature(models.Model):
    name = models.CharField(max_length=200, blank=False)
    calm = models.IntegerField(default=0)
    playful = models.IntegerField(default=0)
    cuddly = models.IntegerField(default=0)
    independent = models.IntegerField(default=0)
    noisy = models.IntegerField(default=0)
    kindness = models.IntegerField(default=0)
    socialness = models.IntegerField(default=0)
    scampish = models.IntegerField(default=0)
    robust = models.IntegerField(default=0)
    sexual_activity = models.IntegerField(default=0)
    tidyness = models.IntegerField(default=0)
    fluffyness = models.IntegerField(default=0)
    dirtyness = models.IntegerField(default=0)
    first_cat = models.IntegerField(default=0)
    originality = models.IntegerField(default=0)
    budget = models.IntegerField(default=0)

    def __unicode__(self):
        return self.name

class Race(models.Model):
    name = models.CharField(max_length=100, blank=False)
    nature = models.ForeignKey(Nature, null=True)
    adult_image = ImageField(upload_to='races', null=True, blank=True)
    young_image = ImageField(upload_to='races', null=True, blank=True)

    def __unicode__(self):
        return self.name

class Cat(models.Model):
    basket = models.ForeignKey(Basket)
    race = models.ForeignKey(Race)
    name = models.CharField(max_length=100, blank=False)

    def __unicode__(self):
        return self.name

def create_user_basket(sender, instance, created, **kwargs):
    if created:
        Basket.objects.create(owner=instance)

post_save.connect(create_user_basket, sender=User)