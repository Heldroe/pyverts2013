from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    #url(r'^(?P<gallery_id>\d+)/$', 'galleries.views.view', name='gallery'),
    #url(r'^(?P<gallery_id>\d+)/item/', include('galleries.items.urls')),
    url(r'^races/$', 'cats.views.json_races'),
    url(r'^create/$', 'cats.views.create', name='create_cat'),
    url(r'^delete/(?P<cat_id>\d+)/$', 'cats.views.delete', name='delete_cat'),
)