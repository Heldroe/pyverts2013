# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Nature.cuddly'
        db.add_column(u'cats_nature', 'cuddly',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Nature.independent'
        db.add_column(u'cats_nature', 'independent',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Nature.noisy'
        db.add_column(u'cats_nature', 'noisy',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Nature.kindness'
        db.add_column(u'cats_nature', 'kindness',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Nature.socialness'
        db.add_column(u'cats_nature', 'socialness',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Nature.scampish'
        db.add_column(u'cats_nature', 'scampish',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Nature.robust'
        db.add_column(u'cats_nature', 'robust',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Nature.sexual_activity'
        db.add_column(u'cats_nature', 'sexual_activity',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Nature.tidyness'
        db.add_column(u'cats_nature', 'tidyness',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Nature.fluffyness'
        db.add_column(u'cats_nature', 'fluffyness',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Nature.dirtyness'
        db.add_column(u'cats_nature', 'dirtyness',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Nature.first_cat'
        db.add_column(u'cats_nature', 'first_cat',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Nature.originality'
        db.add_column(u'cats_nature', 'originality',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Nature.budget'
        db.add_column(u'cats_nature', 'budget',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Nature.cuddly'
        db.delete_column(u'cats_nature', 'cuddly')

        # Deleting field 'Nature.independent'
        db.delete_column(u'cats_nature', 'independent')

        # Deleting field 'Nature.noisy'
        db.delete_column(u'cats_nature', 'noisy')

        # Deleting field 'Nature.kindness'
        db.delete_column(u'cats_nature', 'kindness')

        # Deleting field 'Nature.socialness'
        db.delete_column(u'cats_nature', 'socialness')

        # Deleting field 'Nature.scampish'
        db.delete_column(u'cats_nature', 'scampish')

        # Deleting field 'Nature.robust'
        db.delete_column(u'cats_nature', 'robust')

        # Deleting field 'Nature.sexual_activity'
        db.delete_column(u'cats_nature', 'sexual_activity')

        # Deleting field 'Nature.tidyness'
        db.delete_column(u'cats_nature', 'tidyness')

        # Deleting field 'Nature.fluffyness'
        db.delete_column(u'cats_nature', 'fluffyness')

        # Deleting field 'Nature.dirtyness'
        db.delete_column(u'cats_nature', 'dirtyness')

        # Deleting field 'Nature.first_cat'
        db.delete_column(u'cats_nature', 'first_cat')

        # Deleting field 'Nature.originality'
        db.delete_column(u'cats_nature', 'originality')

        # Deleting field 'Nature.budget'
        db.delete_column(u'cats_nature', 'budget')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'cats.basket': {
            'Meta': {'object_name': 'Basket'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'owner': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'cats.cat': {
            'Meta': {'object_name': 'Cat'},
            'basket': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cats.Basket']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'race': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cats.Race']"})
        },
        u'cats.nature': {
            'Meta': {'object_name': 'Nature'},
            'budget': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'calm': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'cuddly': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'dirtyness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'first_cat': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'fluffyness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'independent': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'kindness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'noisy': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'originality': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'playful': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'robust': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'scampish': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'sexual_activity': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'socialness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'tidyness': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'cats.race': {
            'Meta': {'object_name': 'Race'},
            'adult_image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nature': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cats.Nature']", 'null': 'True'}),
            'young_image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['cats']